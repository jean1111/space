// http module should be installed:
// npm i http
let axios = require('axios');
let cheerio = require('cheerio');
let FormData = require('form-data');
// let request = require('request');
// let fs = require('fs');
const url = "https://www.spensiones.cl/apps/certificados/formConsultaAfiliacion.php";

// Params:
// your anti-captcha.com account key
var anticaptcha = require('./anticaptcha')('12cf1c9539189ca33e32b3ada313c8b5');

//recaptcha key from target website
anticaptcha.setWebsiteURL(url);
anticaptcha.setWebsiteKey("6LefRUMUAAAAAGtRLa7E4jdPmjq6smsHNzwZZN7P");

//browser header parameters
anticaptcha.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116");


axios.get(url).then((response) => {
	const body = response.data;
	return(body);
}).then ((body) => {
	let $ = cheerio.load(body);
	let sessionid = $('input[type="hidden"]').val();
	console.log("sessionid: "+sessionid);

	anticaptcha.getBalance(function (err, balance) {
		if (err) {
			console.error(err);
			return;
		}

		if (balance > 0) {
			anticaptcha.createTaskProxyless(function (err, taskId) {
				if (err) {
					console.error(err);
					return;
				}

				console.log("taskId: "+taskId);

				anticaptcha.getTaskSolution(taskId, function (err, taskSolution) {
					if (err) {
						console.error(err);
						return;
					}

					console.log("taskSolution: "+taskSolution);


					//Hacer peticion con los datos a consultar
					let bodyFormData = new FormData();
					bodyFormData.append('sessionid', sessionid);
					bodyFormData.append('tipocons', '0');
					bodyFormData.append('rut', '180243062');
					bodyFormData.append('g-recaptcha-response', taskSolution);

					
					bodyFormData.submit('https://www.spensiones.cl/apps/certificados/consultaAfiliacion.php', function(err, res) {
  						
  						if (err) throw err;
  						console.log(res.method, res.url, res.headers);
  						let body = "";
  						res.on('readable', function() {
  							body += res.read();
  						});
  						res.on('end', function() {
  							// console.log(body);
  							const $ = cheerio.load(body);
  							let contenido = $('.presentacion-app').text();
  							console.log(contenido);
  						});
					});
					//FIN
				});
			});
		}
	});

});
